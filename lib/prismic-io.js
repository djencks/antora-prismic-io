'use strict'

const handlebars = require('handlebars')
const { posix: path } = require('path')
const prismic = require('prismic-javascript')
const requireFromString = require('require-from-string')

const events = require('./constants')
const convert = require('./prismic-asciidoc')
const RemoteFetch = require('@djencks/antora-remote-fetch')
const PROJECT_NAME = 'prismic-io'

const DEBUG = false

const TYPE_RX = /^\[at\(document.type,[ ]*"([^" ]*)"\)]$/

module.exports.register = (eventEmitter, config = {}) => {
  //config is from playbook entry for this extension.
  //Config consists of a field queries containing a list of objects.
  //Each object contains four fields to locate the template,
  // and information about the query to be used with the template.
  // component
  // version
  // module
  // relative (expected to be in examples, an hbs template)
  // url for the prismic.io repository, ending in api/v2.
  // query: one or an array of predicates. Each predicate must be surrounded by []. Literals must be quoted with "".
  // options for the query, as in prismic documentation: orderings and after.
  // navIndex for nav files (which don't have a query) to specify the nav file order.
  //In addition, there's a helpers field
  // helpers a list of component-version-module with a map of helper name to relative.  The example family is assumed.
  const queries = config.queries || []

  //caching configuration
  const allowHttp = config.allowHttp
  const rejectUnauthorized = 'rejectUnauthorized' in config ? !!config.rejectUnauthorized : true
  const allowedFilePaths = config.allowedFilePaths || []
  const noCache = config.noCache

  const resultMap = {}
  const typeMap = {}
  const navMap = {}

  eventEmitter.on(events.AFTER_CLASSIFY_CONTENT,
    async (playbook, contentCatalog) => {
      //return a promise or undefined

      //set up cache location
      const startDir = playbook.dir || '.'
      const { cacheDir, fetch } = playbook.runtime

      const options = {
        fetch,
        allowHttp,
        rejectUnauthorized,
        allowedFilePaths,
      }
      if (!noCache) {
        options.cacheInfo = {
          preferredCacheDir: cacheDir,
          projectDir: PROJECT_NAME,
          startDir,
        }
      }
      const remoteFetch = new RemoteFetch(options)
      await remoteFetch.initialize()

      const converter = convert(linkResolver, contentCatalog, remoteFetch)
      handlebars.registerHelper('asAsciidoc', converter.asAsciidoc)
      handlebars.registerHelper('asText', converter.asText)
      handlebars.registerHelper('ifeq', converter.ifeq)
      handlebars.registerHelper('listLinksFromQuery', listLinksFromQuery)
      handlebars.registerHelper('query', queryHelper)
      handlebars.registerHelper('wrapEmbed', converter.wrapEmbed)
      handlebars.registerHelper('wrapImage', converter.wrapImage)
      config.helpers.forEach(({ version, component, module, helpers }) => {
        Object.entries(helpers).forEach(([name, relative]) => {
          const file = contentCatalog.getById({ version, component, module, family: 'example', relative })
          if (file) {
            handlebars.registerHelper(name, requireFromString(file.contents.toString()))
          } else {
            console.log(`alleged helper ${version}@${component}:${module}:example$${relative} not found in content catalog`)
          }
        })
      })

      const apis = {}
      for (const { url } of queries.filter(({ url }) => url)) {
        apis[url] || (apis[url] = await prismic.api(url, {}))
      }
      await Promise.all(queries.map(async (queryInfo) => {
        const { component, version, module, relative, url, query, options, navIndex } = queryInfo
        if (!contentCatalog.getComponentVersion(component, version)) {
          return console.log(`no such component version ${version}@${component}, skipping query`)
        }
        let template
        if (relative) {
          const templateFile = contentCatalog.getById({
            component,
            version,
            module,
            family: 'example',
            relative,
          })
          if (!templateFile) {
            return console.log(`template not found: ${version}@${component}:${module}:example$${relative}`)
          }
          DEBUG && console.log('template dirname', path.dirname(relative))
          template = handlebars.compile(templateFile.contents.toString(), {
            preventIndent: true,
          })
        }
        const key = getKey(queryInfo)
        if (navIndex) {
          navMap[key] = { template, component, version, module, relative, navIndex }
        } else {
          const queryArray = Array.isArray(query) ? query : [query]
          for (const predicate of queryArray) {
            const match = predicate.match(TYPE_RX)
            if (match) {
              const type = match[1]
              DEBUG && console.log(`predicate ${predicate} has type ${type}`)
              typeMap[type] || (typeMap[type] = key)
              break
            }
          }
          let dirname
          if (relative) {
            dirname = path.dirname(relative)
            dirname = dirname === '.' ? '' : dirname + '/'
          }
          const api = apis[url]
          const workingOptions = Object.assign({ pageSize: 100 }, options)
          const firstResults = await api.query(queryArray, workingOptions)
          DEBUG && console.log('results', JSON.stringify(firstResults, null, '  '))
          let results
          if (firstResults.total_pages > 1) {
            const resultsArray = [firstResults.results]
            for (let i = 2; i <= firstResults.total_pages; i++) {
              workingOptions.page = i
              resultsArray.push((await api.query(queryArray, workingOptions)).results)
            }
            results = [].concat(...resultsArray)
          } else {
            results = firstResults.results
          }
          resultMap[key] = { results, template, component, version, module, relative, dirname }
        }
      }
      ))
      Object.values(resultMap)
        .filter(({ template }) => template)
        .forEach(({ results, template, component, version, module, relative, dirname }) => {
          results.forEach((doc) => {
            DEBUG && console.log('\ndata\n', JSON.stringify(doc.data, null, '  '))
            const adoc = template(doc, { data: { resourceId: { component, version, module, relative, dirname } } })
            DEBUG && console.log(`converted: '${adoc}'`)
            const page = {
              contents: Buffer.from(adoc),
              mediaType: 'text/asciidoc',
              path: doc.uid + '.adoc',
              src: {
                family: 'page',
                mediaType: 'text/asciidoc',
                component,
                version,
                module,
                relative: `${dirname}${doc.uid || path.basename(relative).slice(0, -4)}.adoc`,
              },
            }
            contentCatalog.addFile(page)
          })
        })
      Object.values(navMap).forEach(({ template, component, version, module, relative, navIndex }) => {
        const adoc = template({}, { data: { resourceId: { component, version, module, relative } } })
        DEBUG && console.log(`converted: '${adoc}'`)
        relative = `${relative.slice(0, -4)}.adoc`
        const page = {
          contents: Buffer.from(adoc),
          mediaType: 'text/asciidoc',
          path: relative,
          src: {
            family: 'nav',
            mediaType: 'text/asciidoc',
            component,
            version,
            module,
            relative,
          },
          nav: { index: navIndex },
        }
        contentCatalog.addFile(page)
      })
      // console.log('remoteFetch.promises', remoteFetch.promises)
      return Promise.all(remoteFetch.promises)
    }
  )

  function getKey ({ version, component, module, relative, id }) {
    return `${version}@${component}:${module}:${relative || id}`
  }

  function linkResolver (link) {
    DEBUG && console.log('linkResolver link:', link)
    const key = typeMap[link.type]
    const info = resultMap[key]
    const relative = link.uid ? `${info.dirname}${link.uid}.adoc` : `${info.relative.slice(0, -4)}.adoc`
    const result = getKey({ version: info.version, component: info.component, module: info.module, relative })
    return result
  }

  function listLinksFromQuery (key, prefix, context) {
    const attributes = Object.entries(context.hash).map(([key, value]) => `${key}="${value}"`).join(',')
    const results = getQueryResults(key, context)
    // console.log('listLinksFromQuery results', results)
    const { version, component, module, relative: rel, dirname } = results
    return results.results.map((doc) => {
      const relative = `${dirname}${doc.uid ? doc.uid : rel.slice(0, -4)}`
      return `${prefix} xref:${getKey({ version, component, module, relative })}.adoc[${attributes}]`
    }).join('\n')
  }

  function getQueryResults (key, context) {
    DEBUG && console.log('queryHelper context', context)
    key = resolveKey(key, context.data.resourceId)
    DEBUG && console.log('queryHelper key', key)
    const result = resultMap[key]
    result || console.log(`no such query with key ${key}`)
    return result
  }

  function queryHelper (key, context) {
    const results = getQueryResults(key, context).results
    DEBUG && console.log('queryHelper results', JSON.stringify((results, null, '  ')))
    return results
  }

  // This is somewhat different than Antora resource ids.
  // The example family is assumed, so it cannot be specified.
  // If a segment is present, say component, all following segments must be present; in this case module and relative.
  // Any missing segments are filled in from the context resourceId.

  // const RESOURCE_ID_RX = /^(?:(?:(?:([^@:$]+)@)?(?:([^@:$]+):)?([^@:$]+))?:)?([^:$][^@:$]*)$/
  const RESOURCE_ID_RX = /^(?:(?:(?:([^@:$]+)@)?([^@:$]+):)?([^@:$]+):)?([^:$][^@:$]*)$/
  const RESOURCE_ID_RX_GROUP = { version: 1, component: 2, module: 3, relative: 4 }

  function resolveKey (partial, resourceId) {
    const match = partial.match(RESOURCE_ID_RX)
    // console.log('match', match)
    const version = match[RESOURCE_ID_RX_GROUP.version] || resourceId.version
    const component = match[RESOURCE_ID_RX_GROUP.component] || resourceId.component
    const module = match[RESOURCE_ID_RX_GROUP.module] || resourceId.module
    const relative = match[RESOURCE_ID_RX_GROUP.relative] || resourceId.relative
    return getKey({ version, component, module, relative })
  }
}
