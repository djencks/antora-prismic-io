'use strict'

const PrismicRichText = require('prismic-richtext')
const Elements = PrismicRichText.Elements
const { Link: LinkHelper } = require('prismic-helpers')

const RESOURCE_ID_RX = /^mailto:(?:([^@:$]+)@)?(?:(?:([^@:$]+):)?(?:([^@:$]+))?:)?(?:([^@:$]+)\$)?([^:$][^@:$]*)$/

const DEBUG = false

function asAsciidoc (linkResolver, contentCatalog, remoteFetch) {
  return (richText, context) => {
    // console.log('2nd parameter', JSON.stringify(context, null, '  '))
    Array.isArray(richText) || (richText = [richText])
    // console.log('asAsciidoc richText', JSON.stringify(richText, null, '  '))
    const result = PrismicRichText.serialize(richText, serialize).join('\n')
    // console.log('asAsciidoc result:', result)
    return result

    function serialize (type, element, content, children) {
      DEBUG && console.log('type', type)
      // console.log(`serialize type: ${type}
      // element: ${JSON.stringify(element, null, '  ')}
      // content: ${content}
      // children: ${JSON.stringify(children, null, '  ')}`)
      switch (type) {
        case Elements.heading1:
          return serializeHeading('=', element, children)
        case Elements.heading2:
          return serializeHeading('==', element, children)
        case Elements.heading3:
          return serializeHeading('===', element, children)
        case Elements.heading4:
          return serializeHeading('====', element, children)
        case Elements.heading5:
          return serializeHeading('=====', element, children)
        case Elements.heading6:
          return serializeHeading('======', element, children)
        case Elements.paragraph:
          return serializeParagraph(element, children)
        case Elements.preformatted:
          return serializePreFormatted(element)
        case Elements.strong:
          return serializeStrong(element, children)
        case Elements.em:
          return serializeEm(element, children)
        case Elements.listItem:
          return serializeUl(element, children)
        case Elements.oListItem:
          return serializeOl(element, children)
        case Elements.list:
          return serializeParagraph(element, children)
        case Elements.oList:
          return serializeParagraph(element, children)
        case Elements.image:
          return serializeImage(element)
        case Elements.hyperlink:
          return serializeHyperlink(element, children)
        case Elements.embed: // embed can be anything, apparently, videos, forms, ???
          return serializeEmbed(element)
        case Elements.label: // maps to role
          return serializeSpan(element, children)
        case Elements.span:
          return serializeSpan(content)
        default:
          console.log('unknown Element: ', type)
          return ''
      }

      //works for 2,3
      function serializeHeading (heading, element, children) {
        return `\n${heading} ${children.join('')}\n`
      }

      //works
      function serializeParagraph (element, children) {
        return `${children.join('')}\n\n`
      }

      function serializePreFormatted (element) {
        // console.log('preformatted', JSON.stringify(element, null, '  '))
        return `....\n${element.text}\n....`
      }

      //These always use **, __ since there's no easy way to discover if we're in the middle of a word,
      // and these make overlapping strong/em regions work properly.
      //works by itself and perhaps with em.
      function serializeStrong (element, children) {
        // console.log('strong, children', children)
        return `**${children.join('')}**`
      }

      //works by itself and perhaps with strong
      function serializeEm (element, children) {
        // console.log('em, children', children)
        var last = children[children.length - 1]
        if (children[0][0] === '**' && last[last.length - 1] === '**') {
          children[0] = `**__${children[0].slice(1)}`
          last = children[children.length - 1]
          children[children.length - 1] = `${last.slice(0, last.length - 1)}__** `
          return children.join('')
        }
        return `__${children.join('')}__`
      }

      // one level works; nested must be done with slices
      function serializeUl (element, children) {
        const level = Number((context.hash.level || '1').slice(-1))
        return `${'*'.repeat(level)} ${children.join('')}\n`
      }

      // one level works; nested must be done with slices
      function serializeOl (element, children) {
        const level = Number((context.hash.level || '1').slice(-1))
        return `${'.'.repeat(level)} ${children.join('')}\n`
      }

      //works
      function serializeImage (image) {
        let attributes = Object.entries(context.hash).map(([key, value]) => `${key}='${value}'`).join(',')
        if (!('width' in context.hash)) {
          attributes = `${image.dimensions.width},${image.dimensions.height},${attributes}`
        }
        const resourceId = remoteFetch.fetchToContentCatalog(image.url, contentCatalog, context.data.resourceId, 'image', 'binary')
        return `\nimage::${resourceId}[${image.alt},${attributes}]\n`
      }

      // works
      function serializeHyperlink (element, children) {
        // console.log('link: element:', element)
        // console.log('link: children:', children)
        const refText = `${children.join('')}`
        const attributes = [refText]
          .concat(...Object.entries(context.hash).map(([key, value]) => `${key}="${value}"`)).join(',')
        const data = element.data
        if (data.link_type === 'Web') {
          if (RESOURCE_ID_RX.test(data.url)) {
            //prismic insists on putting 'mailto:' at the front of resource ids, so we trim it off.
            return `xref:${data.url.slice(7)}[${attributes}]`
          } else {
            return `link:${data.url}[${attributes}]`
          }
        } else if (data.link_type === 'Document') {
          return `xref:${LinkHelper.url(
          data,
          linkResolver
        )}[${refText}]`
        } else if (data.link_type === 'Media') {
          if (data.kind === 'image') {
            const resourceId = remoteFetch.fetchToContentCatalog(data.url, contentCatalog, context.data.resourceId, 'image', 'binary')
            const relative = resourceId.slice(resourceId.indexOf('$') + 1)
            return `link:{imagesdir}/${relative}[${attributes}]`
          } else if (data.kind === 'document') {
            const resourceId = remoteFetch.fetchToContentCatalog(data.url, contentCatalog, context.data.resourceId, 'attachment', 'binary')
            const relative = resourceId.slice(resourceId.indexOf('$') + 1)
            return `link:{attachmentsdir}/${relative}[${attributes}]`
          } else {
            console.log(`Media kind ${data.kind} NYI`)
          }
        } else {
          console.log(`${data.link_type} NYI`)
        }
      }

      //works for youtube
      function serializeEmbed (element) {
        return `.${element.oembed.title}
--
pass:[${element.oembed.html}]
--

`
      }

      //not clear what this could do.  Children seems to be always undefined.
      function serializeSpan (element, children) {
        // console.log('span: element', element)
        // console.log('span: children', children)
        return element
      }
    }
  }
}

module.exports = (linkResolver, contentCatalog, remoteFetch) => {
  const exports = {
    asText: (structuredText, context) => {
      // console.log('2nd parameter', JSON.stringify(context, null, '  '))
      // console.log('asText structured text', structuredText)
      return PrismicRichText.asText(structuredText, null).trim()
    },

    asAsciidoc: asAsciidoc(linkResolver, contentCatalog, remoteFetch),

    // Elements: Elements,

    ifeq: function (a, b, options) {
      if (a === b) {
        return options.fn(this)
      }
      return options.inverse(this)
    },

    wrapEmbed: function (oembed, context) {
      const wrapped = [{ type: 'embed', oembed }]
      return exports.asAsciidoc(wrapped, context)
    },

    wrapImage: function (image, context) {
      const wrapped = [Object.assign({ type: 'image' }, image)]
      return exports.asAsciidoc(wrapped, context)
    },
  }
  return exports
}
